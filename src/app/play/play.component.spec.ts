import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Cell, PlayComponent, Step} from './play.component';

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlayComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#equal', () => {
    it('should be false', () => {
      expect(component.equal(Cell.player1, Cell.player1, Cell.player1, Cell.player2)).toBe(false);
    });

    it('should be true', () => {
      expect(component.equal(Cell.player2, ...(new Array(3).fill(Cell.player2)))).toBe(true);
    });
  });

  describe('#determineWinner', () => {
    it('should be return "КРЕСТИКИ"', () => {
      expect(component.determineWinner(Cell.player1)).toBe('КРЕСТИКИ');
    });
    it('should be return "НОЛИКИ"', () => {
      expect(component.determineWinner(Cell.player2)).toBe('НОЛИКИ');
    });
  });

  describe('#stopGame', () => {
    it('winner X', () => {
      component.stopGame(Cell.player1);
      expect(component.message).toBe('ПОБЕДИЛИ КРЕСТИКИ');
    });
    it('winner O', () => {
      component.stopGame(Cell.player2);
      expect(component.message).toBe('ПОБЕДИЛИ НОЛИКИ');
    });
  });

  describe('#hasWinner', () => {
    let field: Cell[];

    beforeEach(() => {
      field = [];
    });

    it('player 1 winner', () => {
      field[0] = Cell.player1;
      field[1] = Cell.player1;
      field[2] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([0, 1, 2]);
    });
    it('player 1 winner', () => {
      field[3] = Cell.player1;
      field[4] = Cell.player1;
      field[5] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([3, 4, 5]);
    });
    it('player 1 winner', () => {
      field[6] = Cell.player1;
      field[7] = Cell.player1;
      field[8] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([6, 7, 8]);
    });
    it('player 1 winner', () => {
      field[0] = Cell.player1;
      field[3] = Cell.player1;
      field[6] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([0, 3, 6]);
    });
    it('player 1 winner', () => {
      field[1] = Cell.player1;
      field[4] = Cell.player1;
      field[7] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([1, 4, 7]);
    });
    it('player 1 winner', () => {
      field[2] = Cell.player1;
      field[5] = Cell.player1;
      field[8] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([2, 5, 8]);
    });
    it('player 1 winner', () => {
      field[0] = Cell.player1;
      field[4] = Cell.player1;
      field[8] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([0, 4, 8]);
    });
    it('player 1 winner', () => {
      field[2] = Cell.player1;
      field[4] = Cell.player1;
      field[6] = Cell.player1;
      expect(component.getWinner(Cell.player1, field)).toEqual([2, 4, 6]);
    });
  });

  describe('#check', () => {
    it('Step player 2', () => {
      component.winnerArray = [];
      component.field = [Cell.empty];
      component.hod = Step.player2;
      component.check(0);
      expect(component.field).toEqual([Cell.player2]);
    });

    it('hod X', () => {
      component.winnerArray = [];
      component.field[1] = Cell.empty;
      component.hod = Step.player1;
      component.check(1);
      expect(component.field[1]).toBe(Cell.player1);
    });

    it('hod X and they winner', () => {
      component.winnerArray = [];
      component.field = [Cell.empty, Cell.player1, Cell.player1];
      component.hod = Step.player1;
      component.check(0);
      expect(component.winnerArray).toEqual([0, 1, 2]);
    });

    it('field should not change if there is a winner', () => {
      component.winnerArray = [0, 1, 2];
      component.field = [];
      component.hod = Step.player2;
      component.check(5);
      expect(component.field.length).toBe(0);
    });

    it('clicking on a busy cell', () => {
      component.winnerArray = [];
      component.field = [Cell.player1];
      component.hod = Step.player2;
      component.check(0);
      expect(component.field).toEqual([Cell.player1]);
    });
  });

  describe('#startGame', () => {
    it('startGame', () => {
      component.startGame();
      expect(component.field).toEqual(new Array(9).fill(Cell.empty));
      expect(component.hod).toBe(Step.player1);
      expect(component.message).toBe('');
      expect(component.winnerArray).toEqual([]);
    });
  });
});
