import {Component} from '@angular/core';

export enum Step {
  player1 = 'Ход Игрока 1',
  player2 = 'Ход Игрока 2',
  nobody = ''
}

export enum Cell {
  empty = '',
  player1 = 'X',
  player2 = 'O',
}

const WinnerLines = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css']
})
export class PlayComponent {
  hod: Step;
  field: Cell[];
  message: string;
  winnerArray: number[];

  constructor() {
    this.setToDefault();
    this.setToShow();
  }

  setToDefault() {
    this.field = new Array(9).fill(Cell.empty);
    this.hod = Step.player1;
    this.message = '';
    this.winnerArray = [];
  }

  setToShow() {
    for (const key of [1, 4, 7]) {
      this.field[key] = Cell.player1;
    }
    for (const key of [0, 5, 6]) {
      this.field[key] = Cell.player2;
    }
    this.winnerArray = [1, 4, 7];
    this.hod = Step.nobody;
  }

  startGame() {
    this.setToDefault();
  }

  check(i: number) {
    if (this.winnerArray.length === 0) {
      if (this.field[i] === Cell.empty) {
        const nextChar: Cell = this.hod === Step.player1 ? Cell.player1 : Cell.player2;
        this.field[i] = nextChar;

        this.winnerArray = this.getWinner(nextChar, this.field);

        if (this.winnerArray.length) {
          this.stopGame(nextChar);
        } else if (!this.field.includes(Cell.empty)) {
          this.stopGame(Cell.empty);
        } else {
          this.hod = this.hod === Step.player1 ? Step.player2 : Step.player1;
        }
      }
    }
  }

  getWinner(cell: Cell, field: Cell[]): number[] {
    for (const line of WinnerLines) {
      if (this.equal(cell, field[line[0]], field[line[1]], field[line[2]])) {
        return line;
      }
    }
    return [];
  }

  equal(char: Cell, ...array: Cell[]): boolean {
    return array.every((item) => item === char);
  }

  stopGame(char: Cell) {
    this.message = this.getMessage(char);
    this.hod = Step.nobody;
  }

  getMessage(winner: Cell): string {
    if (winner === Cell.empty) {
      return 'НИЧЬЯ';
    } else {
      return 'ПОБЕДИЛИ ' + this.determineWinner(winner);
    }
  }

  determineWinner(value: Cell): string {
    if (value === Cell.player1) {
      return 'КРЕСТИКИ';
    } else {
      return 'НОЛИКИ';
    }
  }
}
